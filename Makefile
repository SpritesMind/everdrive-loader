include makerules 

COMMON_SRCS = $(wildcard $(ARGTABLE3)$(SEP)*.c)
COMMON_SRCS += $(wildcard $(SRCDIR)$(SEP)utils$(SEP)*.c)
COMMON_OBJS = $(patsubst %.c,%.o,$(COMMON_SRCS))
COMMON_RELEASE =  $(patsubst $(SRCDIR)$(SEP)%,$(OBJDIR)$(SEP)%,$(COMMON_OBJS))

SRCS = $(wildcard $(SRCDIR)$(SEP)*.c)
OBJS = $(patsubst %.c,%.o,$(SRCS))
OBJ_RELEASE =  $(patsubst $(SRCDIR)$(SEP)%,$(OBJDIR)$(SEP)%,$(OBJS))

all: release

release: libs folders $(OBJ_RELEASE) $(COMMON_RELEASE)
	$(LD) -o usbload $(OBJ_RELEASE) $(COMMON_RELEASE) $(LDFLAGS) $(LIBDIR) $(LIB)

$(OBJDIR)$(SEP)%.o: $(SRCDIR)$(SEP)%.c
	$(CC) $(CFLAGS) $(INC) -c -o $@ $<


libs: $(ARGTABLE3)$(SEP)LICENSE $(EDLIB)$(SEP)libeverdrive.a 

$(EDLIB)$(SEP)libeverdrive.a: $(EDLIB)$(SEP)Makefile
	$(MAKE) -C $(EDLIB)

$(ARGTABLE3)$(SEP)LICENSE:
	$(error Argtable 3 library missing, please update git submodules)	

clean: 
	@echo Clean release files and folders
	-$(RM) $(COMMON_RELEASE) $(HIDE_ERROR)
	-$(RMD) $(OBJDIR) $(HIDE_ERROR)
	@echo Clean lib
	$(MAKE) -C $(EDLIB) clean
	
folders: $(OBJDIR) 

$(OBJDIR):
	$(MKDIR) $@
	$(MKDIR) $@$(SEP)utils
	
.PHONY: all release clean folders
