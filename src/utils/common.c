#include <stdio.h>
#include <string.h>
#include <ctype.h>
#include "common.h"

// my own case insensitive string compare function, to be portable
int stricmp(char const *str1, char const *str2)
{
	int result = 0;
	const char *a = str1;
	const char *b = str2;
    for (;; a++, b++)
	{
        result = tolower(*a) - tolower(*b);
		//if a & b are same size, !*a handle it
		//if a is longer than b, result!= 0 handle it since result = (*a)-0 (positive)
		//if b is longer than a, result!= 0 handle it since result = 0-(*b) (negative)
        if ( (result != 0) || (!*a) )
            break;
    }
	return result;
}


char file_isValid(const char *filename)
{
	FILE *handle;
	handle = fopen(filename, "rb");
	if (handle == NULL) return 0;
	
	fclose(handle);
	return 1;
}


long file_getSize(const char *filename)
{
    long retValue;
	FILE *handle;

    if (!file_isValid(filename))    return 0L;

	handle = fopen(filename, "rb");
	fseek(handle, 0L, SEEK_END);
    retValue = ftell(handle);

    fseek(handle, 0L, SEEK_SET);
	fclose(handle);

	return retValue;
}

char *file_getExtension(const char *filename) {
	char *dot = strrchr(filename, '.');
	if (!dot || dot == filename) return "";
	return (dot + 1);
}

char file_isFileType(const char *filename, char *fileType)
{
    char *ext = file_getExtension(filename);
    return ( strncmp(ext, fileType, strlen(fileType)) == 0 );
}


void file_getNewFilename(const char *source, char *dest, const char *ext)
{
	strcpy(dest, source);

	char *pFile = strrchr(dest, '/');
	pFile = ((pFile == NULL) ? dest : pFile + 1);
	// change extension
	char *pExt = strrchr(pFile, '.');
	if (pExt != NULL)
		strcpy(pExt, ext);
	else
		strcat(pFile, ext);
}

long file_readToBuffer(const char *filename, void *buffer, long buffersize)
{
	long fileSize = 0;
	FILE *handle;
	if (buffer == NULL)	return 0;
	if (!file_isValid(filename))	return 0;
	fileSize = file_getSize(filename);
	if (fileSize > buffersize)	fileSize = buffersize;
	
	handle = fopen(filename, "rb");
	fread(buffer, fileSize, 1, handle);
	fclose(handle);
	
	return fileSize;
	
}


