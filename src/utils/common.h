#ifndef __COMMON_H__
#define __COMMON_H__

#ifdef __cplusplus
extern "C" {
#endif

int stricmp(char const *str1, char const *str2);

char file_isValid(const char *filename);
long file_getSize(const char *filename);
char *file_getExtension(const char *filename);
char file_isFileType(const char *filename, char *fileType);
void file_getNewFilename(const char *source, char *dest, const char *ext);
long file_readToBuffer(const char *filename, void *buffer, long buffersize);


#ifdef __cplusplus
}
#endif

#endif
