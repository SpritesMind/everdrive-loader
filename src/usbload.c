#include <stdio.h>
#include <stdlib.h>
#include <strings.h>
#include <everdrive.h>
#include <argtable3.h>

#include "utils/common.h"

#define ERROR_BUFFER_SIZE 4

struct arg_lit *help, *version, *info;
struct arg_file *file;
struct arg_int *port, *baudrate;
struct arg_str *portName, *mode;
struct arg_end *end;




static void printver()
{
	printf("USB Loader v1.0\n");
}

int main(int argc, char *argv[])
{
	int exitcode = 0;
	int err;
	everdrive ed;
	unsigned char loadMode = MODE_UNKNOWN;
	char *rom = NULL;
	long rom_size;

	void *argtable[] = {
        file  	= arg_file0(NULL, NULL, "<filename>","file to load"),
        version = arg_litn(NULL, "version", 0, 1, "display version info"),
        help    = arg_litn(NULL, "help", 0, 1, "display this help"),
        info    = arg_litn(NULL, "info", 0, 1, "display EverDrive info"),
        mode    = arg_str0("m", "mode", "<string>","load mode : MD,SMS,OS,SSF (default : detect)"),
        port    = arg_int0("p", "port", "<value>","COM port id"),
        portName= arg_str0("n", "portName", "<string>","COM port name"),
        baudrate= arg_int0("b", "baudrate", "<value>","COM port speed (default : 115200)"),
        end     = arg_end(ERROR_BUFFER_SIZE),
    };
	
	ed.port = AUTODETECT_PORT;
	
	if (arg_nullcheck(argtable) != 0)
	{
		printf("error: insufficient memory\n");
		exitcode = -1;
		goto exit;
    }

	port->ival[0]=AUTODETECT_PORT;
	baudrate->ival[0]=DEFAULT_BAUDRATE;
    err = arg_parse(argc,argv,argtable);
	
    // special case: '--help' takes precedence over error reporting
    if (help->count > 0)
    {
		printver();
        printf("Usage:\nusbload");
        arg_print_syntax(stdout, argtable, "\n");
        arg_print_glossary(stdout, argtable, "  %-25s %s\n");
        printf("If no port info is provided, everdrive will be autodetected\n");
		exitcode = 0;
		goto exit;
    }
	
	// special case 2
	if (version->count)
	{
		printver();
		exitcode = 0;
		goto exit;
	}
	
	
	// If the parser returned any errors then display them and exit
    if ((err > 0) || (argc == 1))
    {
        // print upto ERROR_BUFFER_SIZE error details contained in the arg_end struct
        arg_print_errors(stdout, end, "USB Loader");
        printf("Try '--help' for more information.\n");
		exitcode = -1;
		goto exit;
    }
	
	if (file->count)
	{
		loadMode = MODE_MEGADRIVE;

		long filesize = file_getSize(file->filename[0]);
		if (filesize == 0)
		{
			printf("Error: invalid file %s (0 bytes)\n", file->filename[0]);	
			exitcode = -3;
			goto exit;		
		}
		
		//from origin .net loader : size should be 64k rounded
		rom_size = filesize;
        if (rom_size % 65536 != 0) rom_size = rom_size / 65536 * 65536 + 65536;
		
		//TODO check rom header for SSF

		if (mode->count == 0)
		{
			char *ext = file_getExtension(file->filename[0]);
			if (stricmp(file->filename[0],"MEGAOS.BIN") == 0)
			{
				if (rom_size <= 0x100000)	loadMode = MODE_OS_v1; //else loaded as megadrive rom
			}
			else if (stricmp(file->filename[0],"MEGA.BIN") == 0)
			{
				loadMode = MODE_OS_v2;
			}
			else if ( stricmp( ext,"RBF" ) == 0)
			{
				if (rom_size <= 0x18000)	loadMode = MODE_FPGA;
			}
			else if ( stricmp( ext,"SMS" ) == 0)
			{
				loadMode = MODE_SMS;
			}
			else if ( stricmp( ext,"32X" ) == 0)
			{
				//??
			}
		}
		else
		{
			if (stricmp(mode->sval[0], "MD") == 0)
			{
				loadMode = MODE_MEGADRIVE;
			}
			else if (stricmp(mode->sval[0], "GEN") == 0)
			{
				loadMode = MODE_MEGADRIVE;
			}
			else if (stricmp(mode->sval[0], "SMS") == 0)
			{
				loadMode = MODE_SMS;
			}
			else if (stricmp(mode->sval[0], "OS") == 0)
			{
				loadMode = MODE_OS_v2;
			}
			else if (stricmp(mode->sval[0], "SSF") == 0)
			{
				loadMode = MODE_SSF;
			}
		}

		
		rom = calloc(1, rom_size);
		if ( file_readToBuffer(file->filename[0], rom, rom_size) == 0)
		{
			printf("Error: invalid file %s (unreadable)\n", file->filename[0]);	
			exitcode = -3;
			goto exit;		
		}
	}
	
	
	if ( !ed_open(&ed, port->ival[0], (portName->count?portName->sval[0]:NULL), baudrate->ival[0] ) )
	{
		if (portName->count)
		{
			printf("Error: Unable to connect to EverDrive on port %s\n", portName->sval[0]);
		}
		else if (port->ival[0] != AUTODETECT_PORT)
		{
			printf("Error: Unable to connect to EverDrive at port %d\n", port->ival[0]);
		}
		else
			printf("Error: No EverDrive found\n");
		
		exitcode = -2;
		goto exit;
	}
	
	if (info->count > 0)
	{
		printf("Connected to EverDrive found at port %d (%s), at %dbps\n", ed.port, ed.portName, ed.baudrate);
		//this doesn't close usbload
	}


	exitcode = 0;

	if (loadMode == MODE_MEGADRIVE)
	{
		if (ed_loadROM(&ed, rom, rom_size))
		{
			if ( !ed_launchROM(&ed, loadMode) )
			{
				printf("Error: Unable to launch game\n");
				exitcode = -4;
			}
		}
		else 
		{
			printf("Error: Can't upload ROM\n");
			exitcode = -4;
		}
	}
	else
	{
		printf("Mode not yet supported %d\n", loadMode);
		exitcode = -4;
	}



exit:
	if (rom != NULL)		free(rom);
	if (ed.port != AUTODETECT_PORT)	ed_close(&ed);
	
    // deallocate each non-null entry in argtable[]
    arg_freetable(argtable, sizeof(argtable) / sizeof(argtable[0]));
    return exitcode;
}